package com.sda.hibernate.first.entity;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@NamedQueries({
        @NamedQuery(
                name = "find_student_by_name",
                query = "select student from Student student where student.firstName = :firstName"
        ),
        @NamedQuery(
                name = "update_student",
                query = "update Student s set s.firstName = :firstName, s.lastName = :lastName, s.email = :email, s.age = :age where s.id = :id"
        ),
        @NamedQuery(
                name = "delete_student",
                query = "delete from Student s where s.id = :id"
        )
})

@Entity
@Table(name = "student")
public class Student {

    private static final String STUDENT_SEQUENCE = "student_id_seq";
    private static final String STUDENT_GENERATOR = "student_generator";
    //vor fi folosite pt crearea generatorului pt id
    //fol un generator pt ca id sa fie mereu diferit, chiar daca cnv introduce din greseala un id desi e pe auto increment

    @Id
    @SequenceGenerator(name = STUDENT_GENERATOR,sequenceName = STUDENT_SEQUENCE)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = STUDENT_GENERATOR)
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "age")
    private int age;

    public Student() {

    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "student")
    private List<Grade> grades;

    @OneToOne(mappedBy = "student", cascade = CascadeType.ALL, orphanRemoval = true)
    private Address address;

    @ManyToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinTable(name = "student_courses",
            joinColumns = @JoinColumn(name = "student_id"),
            inverseJoinColumns = @JoinColumn(name = "course_id"))
    private List<Course> courses;

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                ", grades=" + grades +
                ", address=" + address +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        Student student = (Student) o;
        return id == student.id &&
                age == student.age &&
                Objects.equals(firstName, student.firstName) &&
                Objects.equals(lastName, student.lastName) &&
                Objects.equals(email, student.email) &&
                Objects.equals(grades, student.grades) &&
                Objects.equals(address, student.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, email, age, grades, address);
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<Grade> getGrades() {
        return grades;
    }

    public void setGrades(List<Grade> grades) {
        this.grades = grades;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }
}
