package com.sda.hibernate.first.entity;


import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "courses")
public class Course {

    private static final String COURSE_SEQUENCE = "course_id_seq";
    private static final String COURSE_GENERATOR = "course_generator";

    @Id
    @SequenceGenerator(name = COURSE_GENERATOR, sequenceName = COURSE_SEQUENCE)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = COURSE_GENERATOR)
    private int id;

    @Column(name = "course_name")
    private String courseName;

    @Column(name = "teacher_name")
    private String teacherName;

    @ManyToMany(mappedBy = "courses",fetch = FetchType.EAGER)
    private List<Student> students;

    public Course() {
    }

    public Course(String courseName, String teacherName) {
        this.courseName = courseName;
        this.teacherName = teacherName;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", courseName='" + courseName + '\'' +
                ", teacherName='" + teacherName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Course)) return false;
        Course course = (Course) o;
        return id == course.id &&
                Objects.equals(courseName, course.courseName) &&
                Objects.equals(teacherName, course.teacherName) &&
                Objects.equals(students, course.students);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, courseName, teacherName, students);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }
}
