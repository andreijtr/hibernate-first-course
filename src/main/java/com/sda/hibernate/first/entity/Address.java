package com.sda.hibernate.first.entity;


import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "address")
public class Address {

    private static final String ADDRESS_SEQUENCE = "address_id_seq";
    private static final String ADDRESS_GENERATOR = "address_generator";

    @Id
    @SequenceGenerator(name = ADDRESS_GENERATOR, sequenceName = ADDRESS_SEQUENCE)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = ADDRESS_GENERATOR)
    private int id;

    @Column(name = "country")
    private String country;

    @Column(name = "city")
    private String city;

    @Column (name = "street")
    private String street;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "student_id")
    private Student student;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Address)) return false;
        Address address = (Address) o;
        return id == address.id &&
                Objects.equals(country, address.country) &&
                Objects.equals(city, address.city) &&
                Objects.equals(street, address.street) &&
                Objects.equals(student, address.student);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, country, city, street, student);
    }
}
