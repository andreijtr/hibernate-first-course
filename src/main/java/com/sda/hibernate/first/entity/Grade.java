package com.sda.hibernate.first.entity;


import javax.persistence.*;
import java.util.Objects;

@Entity
@Table (name = "grades")
public class Grade {
    private static final String GRADE_SEQUENCE = "grade_id_seq";
    private static final String GRADE_GENERATOR = "grade_generator";

    @Id
    @SequenceGenerator(name = GRADE_GENERATOR,sequenceName = GRADE_SEQUENCE)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GRADE_GENERATOR)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "value")
    private int value;

    @ManyToOne()
    @JoinColumn(name = "id_student")
    private Student student;

    public Grade() {
    }

    public Grade (String name, int value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String toString() {
        return "Grade{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Grade)) return false;
        Grade grade = (Grade) o;
        return id == grade.id &&
                value == grade.value &&
                Objects.equals(name, grade.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, value);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
