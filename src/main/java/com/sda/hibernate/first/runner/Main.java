package com.sda.hibernate.first.runner;

import com.sda.hibernate.first.database.AddressDao;
import com.sda.hibernate.first.database.StudentDao;
import com.sda.hibernate.first.entity.Address;
import com.sda.hibernate.first.entity.Course;
import com.sda.hibernate.first.entity.Grade;
import com.sda.hibernate.first.entity.Student;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
//
//        Student student2 = new Student();
//        student2.setFirstName("marius");
//        student2.setLastName("numaru");
//        student2.setAge(13);
//        student2.setEmail("mariusijeqtre@gmail.com");
//
        List<Student> students = new LinkedList<>();
//        students.add(student);
//        students.add(student2);
//
//        studentDao.insertStudents(students);

        //studentDao.insertStudent(student);
        //System.out.println(studentDao.findStudentById(452));
        //student.setId(452);
        //student.setFirstName("becali");
        //studentDao.updateStudent(student);
        //studentDao.deleteStudent(452);

        //System.out.println(studentDao.findStudentByFirstName("micu"));
        //studentDao.updateStudentUsingNamedQuery("maria", "elena", "lenuta@yahoo.com", 29, 702);
        //studentDao.deleteStudentUsingNamedQuery(702);

//        List<Student> myList = studentDao.findStudentListByFirstName("ghita");
//
//        for (Student s : myList) {
//            System.out.println(s);
//        }

        AddressDao addressDao = new AddressDao();
        StudentDao studentDao = new StudentDao();

        Address address = new Address();
        Student student = new Student();

        students.add(student);

        address.setCountry("Romania");
        address.setCity("Iasi");
        address.setStreet("Palat");
        address.setStudent(student);

        List<Grade> grades = Arrays.asList(
                new Grade("Matematica", 10),
                new Grade ("Lb. romana", 7),
                new Grade ("infor", 9)
        );

        student.setFirstName("Andrei");
        student.setLastName("Jitaru");
        student.setAge(43);
        student.setEmail("andreijtre@gmail.com");
        student.setAddress(address);
        student.setGrades(grades);

        for(Grade grade : grades) {
            grade.setStudent(student);
        }


        List<Course> courses = Arrays.asList(
                new Course("Matematica", "Tibi Guru"),
                new Course("Lb romana", "Gica gicuta"));

        student.setCourses(courses);

        for (Course course : courses) {
            course.setStudents(students);
        }

        studentDao.insertStudent(student);
    }
}
