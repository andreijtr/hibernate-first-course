package com.sda.hibernate.first.database;

import com.sda.hibernate.first.entity.Student;

import javax.persistence.Query;
import java.util.List;

public class StudentDao extends DbInitializer {

    public void insertStudent(Student student) {
        openSessionAndTransaction();
        session.persist(student);
        student.setFirstName("ghita");
        closeSessionAndTransaction();
    }

    public Student findStudentById(int id) {
        openSessionAndTransaction();
        Student student = session.find(Student.class, id);
        closeSessionAndTransaction();
        return student;
    }

    public void updateStudent(Student student) {
        openSessionAndTransaction();
        session.update(student);
        closeSessionAndTransaction();
    }

    public void deleteStudent(int id) {
        openSessionAndTransaction();
        Student student = session.find(Student.class, id);
        session.delete(student);
        closeSessionAndTransaction();
    }

    public Student findStudentByFirstName (String firstName) {
        openSessionAndTransaction();

        Query query = session.createNamedQuery("find_student_by_name");
        query.setParameter("firstName", firstName);
        Student student = (Student) query.getSingleResult();
        closeSessionAndTransaction();

        return student;
    }

    public void updateStudentUsingNamedQuery(String firstName, String lastName, String email, int age, int id) {
        openSessionAndTransaction();

        Query query = session.createNamedQuery("update_student");
        query.setParameter("firstName", firstName);
        query.setParameter("lastName", lastName);
        query.setParameter("email", email);
        query.setParameter("age", age);
        query.setParameter("id", id);
        query.executeUpdate();

        closeSessionAndTransaction();
    }

    public void deleteStudentUsingNamedQuery(int id) {
        openSessionAndTransaction();
        Query query = session.createNamedQuery("delete_student");
        query.setParameter("id", id);
        query.executeUpdate();
        closeSessionAndTransaction();
    }

    public List<Student> findStudentListByFirstName(String firstName) {
        List<Student> students;
        openSessionAndTransaction();
        Query query = session.createNamedQuery("find_student_by_name");
        query.setParameter("firstName", firstName);
        students = query.getResultList();
        return students;
    }

    public void insertStudents(List<Student> students) {
        openSessionAndTransaction();
        for (Student s : students) {
            try {
                session.persist(s);
            } catch (Exception e) {
                transaction.rollback();
            }
        }
        closeSessionAndTransaction();
    }
}
