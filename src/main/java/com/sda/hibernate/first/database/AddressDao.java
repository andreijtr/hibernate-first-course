package com.sda.hibernate.first.database;

import com.sda.hibernate.first.entity.Address;

public class AddressDao extends DbInitializer {

    public void insertAddress(Address address) {
        openSessionAndTransaction();
        session.persist(address);
        closeSessionAndTransaction();
    }
}
